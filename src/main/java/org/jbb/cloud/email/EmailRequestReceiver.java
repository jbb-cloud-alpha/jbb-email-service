package org.jbb.cloud.email;

import lombok.extern.slf4j.Slf4j;
import org.jbb.cloud.email.api.v1.Email;
import org.jbb.cloud.email.api.v1.EmailConstraints;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmailRequestReceiver {

  @RabbitListener(queues = EmailConstraints.EMAIL_DELIVERY_QUEUE_NAME)
  public void receiveEmailToSend(Email emailToSend) {
    log.info("Sending email to {}", emailToSend.getRecipientEmail());
  }

}
